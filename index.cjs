const fs = require("fs");
const http = require("http");
const { v4: uuidv4 } = require("uuid");

function allProblems() {
  const server = http.createServer((request, response) => {
    let requestedUrl = request.url;
    let status_code = Number(requestedUrl.split("/").slice(-1).join(""));
    let delay_in_seconds = Number(requestedUrl.split("/").slice(-1).join(""));

    if (requestedUrl == "/html") {
      fs.readFile("./index.html", "utf8", (errorGot, dataGot) => {
        if (errorGot) {
          console.error("Error:");
          console.error(errorGot);
        } else {
          response.writeHead(200, { "Content-Type": "text/html" });
          response.write(dataGot);
          response.end();
        }
      });
    } else if (requestedUrl == "/json") {
      fs.readFile("./index.json", "utf8", (errorGot, dataGot) => {
        if (errorGot) {
          console.error("Error:");
          console.error(errorGot);
        } else {
          response.writeHead(200, { "Content-Type": "text/json" });
          response.write(dataGot);
          response.end();
        }
      });
    } else if (requestedUrl == "/uuid") {
      let uuidGenerated = uuidv4();
      let objectCreated = {
        uuid: uuidGenerated,
      };
      response.writeHead(200, { "Content-Type": "text/json" });
      response.write(JSON.stringify(objectCreated));
      response.end();
    } else if (requestedUrl == `/status/${status_code}`) {
      response.writeHead(status_code, { "Content-Type": "text/json" });
      response.write(
        `/status/${status_code} - ${http.STATUS_CODES[status_code]}`
      );
      response.end();
    } else if (requestedUrl == `/delay/${delay_in_seconds}`) {
      setTimeout(() => {
        response.writeHead(200, { "Content-Type": "text/json" });
        response.write(`Success`);
        response.end();
      }, delay_in_seconds * 1000);
    } else {
      response.end("Invalid Request!");
    }
  });
  server.listen(8000);
  console.log("Node.js web server at port 8000 is running..");
}

allProblems();
